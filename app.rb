require 'rubygems'
require 'sinatra'
require 'byebug'
require 'json'

configure do
  file = File.new("#{settings.root}/log/#{settings.environment}.log", 'a+')
  file.sync = true
  use Rack::CommonLogger, file
end

get '/' do
  puts 'sup bruh'
  'sup bruh'
end

post '/slack-police' do
  content_type :json

  def message_from_slack_police?
    params[:user_name] == 'slack-police' ? true : false
  end

  if !message_from_slack_police?

    slack_username = params[:user_name]
    slack_channel = params[:channel_name]
    slack_message = params[:text]

    slack_message.downcase.gsub!(/[^A-Za-z. ]/, '')
    slack_message.prepend(' ')

    all = {}

    all['broncofusion'] = [
                           'fusion',
                           'broncofusion',
                          ]

    all['hackpoly2016'] = [
                           'hackpoly',
                           'hackathon',
                           'hacking',
                           'prizes',
                           'sponsor'
                          ]

    all['startupchallenge2016'] = [
                                   'challenge',
                                   'startup',
                                   'broncostartupchallenge',
                                   'startupchallenge'
                                  ]

    all['website'] = [
                      'website',
                      'polyfounders website',
                      'site'
                     ]

    # remove array of keywords from hash depending on what channel
    all.delete(slack_channel)

    # prepare array of all comparable words
    compare = []

    all.each do |_,v|
      # value here is an array
      v.each do |word|
        compare.push word
      end
    end

    compare.each do |word|
      if slack_message.include? word
        formatted_username = slack_username
        formatted_channel = slack_channel

        message =
          ":rotating_light::police_car::oncoming_police_car::rotating_light::police_car::oncoming_police_car::rotating_light::police_car::oncoming_police_car::rotating_light::police_car::oncoming_police_car::rotating_light:\
          \nYO!!!!!!!!!!! THIS IS THE SLACK POLICE. \
          \n:rotating_light::police_car::oncoming_police_car::rotating_light::police_car::oncoming_police_car::rotating_light::police_car::oncoming_police_car::rotating_light::police_car::oncoming_police_car::rotating_light:\
          \n#{formatted_username.upcase}, GET ON THE RIGHT CHANNEL!!!!!!!!!!!! \
          \n:rotating_light::police_car::oncoming_police_car::rotating_light::police_car::oncoming_police_car::rotating_light::police_car::oncoming_police_car::rotating_light::police_car::oncoming_police_car::rotating_light:\
          \nDON'T BE DISCUSSING #{word.upcase} ON ##{formatted_channel.upcase} BRUH. \
          \n:rotating_light::police_car::oncoming_police_car::rotating_light::police_car::oncoming_police_car::rotating_light::police_car::oncoming_police_car::rotating_light::police_car::oncoming_police_car::rotating_light:"

        json_response = {
                          'username': 'slack-police',
                          'text': message,
                          'link_names': 1
                        }.to_json
        return json_response
      end
    end

  end
  return {}.to_json

end
